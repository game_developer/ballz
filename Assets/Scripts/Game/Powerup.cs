﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Powerup : MonoBehaviour 
{
	private bool used = false;

	private void Start()
	{
		Ball.LastBallReady += OnLastBallReady;
	}

	private void OnLastBallReady()
	{
		if (used)
		{
			Die();
		}
	}

	protected List<int> ballIDs = new List<int>();

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Ball") && !ballIDs.Contains(collision.GetInstanceID()))
		{
			ballIDs.Add(collision.GetInstanceID());

			UsePowerUp(collision.gameObject);

			used = true;
		}

		Debug.Log("ORIGINAL");
	}

	private void OnDestroy()
	{
		Ball.LastBallReady -= OnLastBallReady;
	}

	protected virtual void Die()
	{
		Destroy(gameObject);
	}

	protected abstract void UsePowerUp(GameObject ball);
}

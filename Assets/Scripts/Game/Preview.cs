﻿using UnityEngine;

public class Preview : MonoSingleton<Preview>
{
	[SerializeField] private GameObject dotPrefab;
	[SerializeField] private int dotsCount = 2;
	[SerializeField] private float gap;

	[SerializeField, Space] private LayerMask layerMask;

	private GameObject[] dots;

	private void Start()
	{
		dots = new GameObject[dotsCount];

		for (int i = 0; i < dotsCount; i++)
		{
			dots[i] = Instantiate(dotPrefab, transform);
		}
	}

	public void Display(Vector2 dir)
	{
		dir = dir.normalized * gap;

		Vector2 lastDotPosition = transform.position;

		for (int i = 0; i < dotsCount; i++)
		{
			Vector2 pos = lastDotPosition + dir;
			RaycastHit2D hit = Physics2D.Raycast(lastDotPosition, dir, gap, layerMask);

			if (hit && hit.collider.isTrigger == false)
			{
				Vector2 moveVector = Vector2.Reflect(dir, hit.normal);

				pos = lastDotPosition + moveVector;
				
				dir = moveVector.normalized * dir.magnitude;
			}

			dots[i].transform.position = pos;
			dots[i].SetActive(true);

			lastDotPosition = dots[i].transform.position;
		}
	}
}

﻿using System.Collections;
using UnityEngine;

public class GreenBall : MonoBehaviour 
{
	CircleCollider2D circleCollider;

	public static event System.Action GreenBallLanded;

	private void OnGreenBallLanded()
	{
		if (GreenBallLanded != null)
			GreenBallLanded();
	}

	private void Start()
	{
		circleCollider = GetComponent<CircleCollider2D>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Ground Trigger"))
		{
			circleCollider.isTrigger = true;

			OnGreenBallLanded();

			StartCoroutine(Die());
		}
	}

	private IEnumerator Die()
	{
		yield return new WaitForSeconds(3);

		Destroy(gameObject);
	}
}

﻿using UnityEngine;

public class Adder : Powerup
{
	protected override void UsePowerUp(GameObject ball)
	{
		BallManager.Instance.AddBalls(1);

		Die();
	}
}

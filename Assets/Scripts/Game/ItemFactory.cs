﻿using UnityEngine;

public class ItemFactory : MonoSingleton<ItemFactory>
{
	public Block blockPrefab;
	public Powerup adderPrefab;
	public Powerup[] powerupPrefabs;

	public Powerup GetAdder(Transform parent, Vector2 pos)
	{
		Powerup adder = Instantiate(adderPrefab, parent);
		adder.transform.localPosition = pos;

		return adder;
	}

	public Powerup GetPowerup(Transform parent, Vector2 pos)
	{
		Powerup powerup = Instantiate(powerupPrefabs[Random.Range(0, powerupPrefabs.Length)], parent);
		powerup.transform.localPosition = pos;

		return powerup;
	}

	public Block GetBlock(Transform parent, Vector2 pos, int level)
	{
		int hp = level;

		switch (Random.Range(0, 2))
		{
			case 0:
				hp = level;
				break;
			case 1:
				hp *= 2;
				break;

			default: throw new UnityException();
		}

		Block block = Instantiate(blockPrefab, parent);

		block.Construct(pos, hp);

		return block;
	}
}
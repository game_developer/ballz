﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Ball : MonoBehaviour 
{
	#region Variables
	private static float speed = 1.3f;

	[HideInInspector] public int TouchesLeft = 1;

	private Rigidbody2D rb;
	#endregion

	#region Events
	public static event System.Action<Ball> BallLanded;
	private void OnBallLanded()
	{
		if (BallLanded != null)
			BallLanded(this);
	}
	public static event System.Action LastBallReady;
	public void OnLastBallReady()
	{
		if (LastBallReady != null)
			LastBallReady();
	}
	#endregion

	private void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.CompareTag("Ground"))
		{
			if (TouchesLeft > 1)
				TouchesLeft--;

			else
			{
				rb.velocity = Vector2.zero;

				Vector2 currentPos = transform.position;
				currentPos.y = BallManager.BALL_Y;
				transform.position = currentPos;

				OnBallLanded();
			}
		}
	}

	public void MoveToFirstBall(bool last = false)
	{
		StartCoroutine(_moveToFirstBall(last));
	}

	private IEnumerator _moveToFirstBall(bool last)
	{
		float moveSpeed = 15;

		while ((Vector2)this.transform.position != BallManager.firstBallLandingPos)
		{
			this.transform.position = Vector2.MoveTowards(this.transform.position, BallManager.firstBallLandingPos,
				moveSpeed * Time.deltaTime);

			yield return null;
		}

		if (last)
			OnLastBallReady();
	}

	public void Launch(Vector2 moveDirection)
	{
		rb.AddForce(moveDirection * speed, ForceMode2D.Impulse);

		TouchesLeft = 1;
	}
}

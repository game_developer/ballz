﻿using UnityEngine;

public class Star : Powerup
{
	protected override void UsePowerUp(GameObject ball)
	{
		GM.Instance.AddStar();

		Die();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Green Ball"))
		{
		}
	}
}

﻿using UnityEngine;

public class Doubler : Powerup
{
	public GameObject greenBallPrefab;
	public float angle = 45;

	protected override void UsePowerUp(GameObject ball)
	{
		Rigidbody2D rigidbody = ball.GetComponent<Rigidbody2D>();

		Vector2 velocity = rigidbody.velocity;
		rigidbody.velocity = Quaternion.Euler(0, 0, angle) * velocity;

		GameObject greenBall = Instantiate(greenBallPrefab, ball.transform.position, Quaternion.identity, transform);
		greenBall.GetComponent<Rigidbody2D>().velocity = Quaternion.Euler(0, 0, -angle) * velocity;
	}
}

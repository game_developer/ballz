﻿using UnityEngine;

public class Spawner : MonoSingleton<Spawner>
{
	#region Variables
	[SerializeField] private GameObject blockContainerPrefab;

	private BlockContainer[] blockContainers;

	private float _highestPointY;
	private const int BLOCK_CONTAINERS_COUNT = 12;

	private int _visualScore = 1;
	private int _highestLevel = 1;

	public int GetScore() { return _visualScore; }
	#endregion

	#region Events
	public event System.Action MovedDown;

	private void OnMovedDown()
	{
		if (MovedDown != null)
			MovedDown();
	}
	#endregion

	private void Start()
	{
		blockContainers = new BlockContainer[BLOCK_CONTAINERS_COUNT];
		_highestPointY = BlockData.startingPointY;

		Initialize();

		BallManager.Instance.FinishedGathering += OnFinishedGathering;
	}

	private void Initialize()
	{
		for (int i = 0; i < BLOCK_CONTAINERS_COUNT; i++)
			PlaceOnTop(i);
	}

	private void OnFinishedGathering()
	{
		for (int i = 0; i < blockContainers.Length; i++)
			if (blockContainers[i].IsEmpty())
				PlaceOnTop(i);

		MoveAllDown();
	}

	private void PlaceOnTop(int blockContainerIndex)
	{
		if (blockContainers[blockContainerIndex] == null)
			blockContainers[blockContainerIndex] = Instantiate(blockContainerPrefab, Vector2.up * _highestPointY, Quaternion.identity, transform).GetComponent<BlockContainer>();

		else blockContainers[blockContainerIndex].transform.position = Vector2.up * _highestPointY;


		blockContainers[blockContainerIndex].Level = _highestLevel;
		blockContainers[blockContainerIndex].CreateStuff();

		_highestPointY += BlockData.CELL_SIZE;
		_highestLevel++;
	}

	private void MoveAllDown()
	{
		foreach (BlockContainer container in blockContainers)
			container.MoveDown();

		_visualScore++;

		BallManager.Instance.IsReadyToLaunch = true;

		OnMovedDown();

		_highestPointY -= BlockData.CELL_SIZE;
	}
}

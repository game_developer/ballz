﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BlockContainer : MonoBehaviour
{
	private List<GameObject> items = new List<GameObject>();

	public int Level = 0;
	
	public void CreateStuff()
	{
		Clear();
		Spawn();
	}

	private void Spawn()
	{
		int blocksCount = BlockData.BLOCKS_PER_CONTAINER;
		
		List<int> availableIndexes = Enumerable.Range(0, blocksCount).ToList();

		var value = availableIndexes[Random.Range(0, availableIndexes.Count)];
		items.Add(ItemFactory.Instance.GetAdder(transform, GetPosition(value)).gameObject);
		availableIndexes.Remove(value);


		value = availableIndexes[Random.Range(0, availableIndexes.Count)];
		items.Add(ItemFactory.Instance.GetBlock(transform, GetPosition(value), Level).gameObject);
		availableIndexes.Remove(value);

		for (int x = 0; x < blocksCount; x++)
		{
			if (!availableIndexes.Contains(x))
				continue;

			if (Random.Range(0, blocksCount) == 0)
				items.Add(ItemFactory.Instance.GetPowerup(transform, GetPosition(x)).gameObject);

			else if (Random.Range(0, 3) == 0)
				items.Add(ItemFactory.Instance.GetBlock(transform, GetPosition(x), Level).gameObject);
		}
	}

	private Vector2 GetPosition(int index)
	{
		return Vector2.right * (BlockData.startingPointX + index * BlockData.CELL_SIZE);
	}

	public bool IsEmpty()
	{
		return (transform.childCount == 0);
	}

	private void Clear()
	{
		foreach (Transform child in transform)
			Destroy(child.gameObject);

		items.Clear();
	}

	public void MoveDown()
	{
		//Removes Empty Items
		items.RemoveAll(item => item == null);

		StartCoroutine(Move_Down());
	}

	private IEnumerator Move_Down()
	{
		float movementSpeed = 3f;

		var currentPos = transform.position;
		var desiredPos = currentPos;
		desiredPos.y -= BlockData.CELL_SIZE;

		foreach (GameObject item in items)
		{
			Block block = item.GetComponent<Block>();

			if (item != null)
			{
				if (block != null)
				{
					var nextPos = block.transform.position;
					nextPos.y -= BlockData.CELL_SIZE;

					StartCoroutine(block.MoveTextDown(movementSpeed, nextPos));
				}
			}

			else throw new UnityException();
		}
		
		while (transform.position != desiredPos)
		{
			transform.position = Vector3.MoveTowards(transform.position, desiredPos, movementSpeed * Time.deltaTime);

			yield return null;
		}
	}
}
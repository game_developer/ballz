﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class BallManager : MonoSingleton<BallManager>
{
	#region Variables
	[SerializeField] private GameObject ballPlaceholder;

	[SerializeField, Space, Header("Balls")] private GameObject ballPrefab;
	[SerializeField] private float ballsLaunchingDelay = 0.5f;
	[SerializeField] TextMeshProUGUI ballsCountText;

	public const float BALL_Y = -4.3f;
	public const float TEXT_Y_POS = 65;
	public static Vector2 firstBallLandingPos;

	private List<Ball> balls = new List<Ball>();
	private List<Ball> toAdd = new List<Ball>();

	private int ballsLanded = 0;
	private int greenBallsFlying = 0;

	private bool _isReadyToLaunch;

	public bool IsReadyToLaunch
	{
		set
		{
			_isReadyToLaunch = value;

			if (value == true)
			{
				if (balls.Count > 0)
				{
					Preview.Instance.transform.position = ballPlaceholder.transform.position;
				}
			}
			else
			{
				Preview.Instance.gameObject.SetActive(false);
			}
		}
	}

	public int BallCount
	{
		get
		{
			return balls.Count;
		}
	}
	#endregion

	#region Events
	public event System.Action BallsLaunched;

	public void OnBallsLaunched()
	{
		if (BallsLaunched != null)
		{
			BallsLaunched();
		}
	}

	public event System.Action FinishedGathering;
	
	private void OnFinishedGathering()
	{
		if (FinishedGathering != null)
			FinishedGathering();

		UnhideBalls();
		SetText(balls.First().transform.position.x);
	}
	#endregion

	private void Start()
	{
		AddBalls(3, true);
				
		SetBallsPos();

		IsReadyToLaunch = true;

		Ball.BallLanded += OnBallLanded;
		Ball.LastBallReady += OnLastBallReady;
		GreenBall.GreenBallLanded += OnGreenBallLanded;
	}

	private void OnGreenBallLanded()
	{
		greenBallsFlying--;

		if (greenBallsFlying == 0 && ballsLanded == BallCount)
		{
			StartCoroutine(Place_BallPlaceholder());
		}
	}

	private void OnLastBallReady()
	{
		StartCoroutine(Place_BallPlaceholder());
	}

	private void Update()
	{
		Vector2 swipe = MobileInput.Instance.SwipeDelta;

		if (swipe.magnitude > MobileInput.Instance.minSwipeMagnitude && _isReadyToLaunch)
		{
			var dir = swipe;

			Preview.Instance.gameObject.SetActive(true);
			Preview.Instance.Display(dir);

			if (MobileInput.Instance.release == true)
			{
				StartCoroutine(LaunchBalls(swipe));

			}
		}

		else
		{
			Preview.Instance.gameObject.SetActive(false);
		}
	}

	private void OnBallLanded(Ball ball)
	{
		ballsLanded++;

		if (ballsLanded == 1)
			firstBallLandingPos = ball.transform.position;

		if (BallCount == 1)
		{
			ball.OnLastBallReady();
		}

		else
		{
			if (ballsLanded != BallCount)
			{
				ball.MoveToFirstBall(false);
			}

			else
			{
				ball.MoveToFirstBall(true);

				ballsLanded = 0;
			}
		}
	}

	private void SetWhiteColor()
	{
		foreach(Ball ball in balls)
		{
			ball.GetComponent<SpriteRenderer>().color = Color.white;
		}
	}

	public void AddBalls(int amount, bool active = false)
	{
		for (int i = 0; i < amount; i++)
		{
			GameObject newBall = Instantiate(ballPrefab, this.transform);
			newBall.SetActive(active);

			switch (active)
			{
				case false:
					toAdd.Add(newBall.GetComponent<Ball>()); break;
				case true:
					balls.Add(newBall.GetComponent<Ball>()); break;
				default:
					break;
			}
		}
	}

	private void SetBallsPos()
	{
		Vector2 textOffsetVector = Vector2.up * 0.5f;
		Vector2 pos = Vector2.up * BALL_Y;

		for (int i = 0; i < balls.Count; i++)
		{
			balls[i].transform.position = pos;
		}
	}

	private void SetText(float xCoordinate)
	{
		ballsCountText.gameObject.SetActive(true);

		var newPos = Camera.main.WorldToScreenPoint(Vector2.right * xCoordinate);
		newPos.y = TEXT_Y_POS;

		ballsCountText.transform.position = newPos;


		ballsCountText.text = "x" + BallCount.ToString();
	}

	private void UnhideBalls()
	{
		if (toAdd.Count > 0)
		{
			foreach (var ball in toAdd)
			{
				ball.gameObject.SetActive(true);
				ball.transform.position = balls.First().transform.position;
			}

			balls.AddRange(toAdd);

			toAdd.Clear();
		}
	}

	private IEnumerator Place_BallPlaceholder()
	{
		const float placeHolderMoveSpeed = 5;
		float ballsMoveSpeed = 20;

		Vector2 direction = balls.Last().transform.position - ballPlaceholder.transform.position;

		Vector2 placeholderVector = direction * 6 / 7;
		Vector2 ballsVector = -direction  / 7;

		Vector2 placeholderEndPos = (Vector2)ballPlaceholder.transform.position + placeholderVector;
		Vector2 ballsEndPos = (Vector2)balls.Last().transform.position + ballsVector;

		while(Vector2.Distance(ballPlaceholder.transform.position, balls.Last().transform.position) > 0.01)
		{
			ballPlaceholder.transform.position = Vector2.Lerp(ballPlaceholder.transform.position, placeholderEndPos, 
				placeHolderMoveSpeed * Time.deltaTime);

			foreach (Ball ball in balls)
			{
				ball.transform.position = Vector2.Lerp(ball.transform.position, ballsEndPos, ballsMoveSpeed * Time.deltaTime);
			}

			yield return null;
		}

		OnFinishedGathering();

		SetWhiteColor();
	}

	private IEnumerator LaunchBalls(Vector2 direction)
	{
		OnBallsLaunched();
		
		IsReadyToLaunch = false;
		direction.Normalize();

		for (int i = 0; i < balls.Count; i++)
		{
			balls[i].Launch(direction);

			yield return new WaitForSeconds(ballsLaunchingDelay);
		}

		ballsCountText.gameObject.SetActive(false);
	}
}

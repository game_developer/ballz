﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInput : MonoSingleton<MobileInput>
{
	[SerializeField] private float minAngle = 10f;
	[HideInInspector] public float minSwipeMagnitude = 50F;

    [HideInInspector] public bool tap, release, hold;
    [HideInInspector] private Vector2 _swipeDelta;

    private Vector2 initialPosition;

	public Vector2 SwipeDelta
	{
		get
		{
			Vector2 minRightDirection = Quaternion.Euler(0, 0, minAngle) * Vector2.right;
			Vector2 minLeftDirection = Quaternion.Euler(0, 0, 180 - minAngle) * Vector2.right;

			float angle = Mathf.Atan2(_swipeDelta.y, _swipeDelta.x) * Mathf.Rad2Deg;

			if (angle > minAngle && angle < 180 - minAngle)
				return _swipeDelta;

			else
				return (angle >= -90 && angle < minAngle ? minRightDirection : minLeftDirection).normalized * _swipeDelta.magnitude; 
		}
	}

    private void Update()
    {
        release = tap = false;
        _swipeDelta = Vector2.zero;

        if (Input.GetMouseButtonDown(0))
        {
            initialPosition = Input.mousePosition;
            hold = tap = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            release = true;
            hold = false;
            _swipeDelta = (Vector2)Input.mousePosition - initialPosition;
        }

        if (hold)
            _swipeDelta = (Vector2)Input.mousePosition - initialPosition;
    }
}

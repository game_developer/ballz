﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GM : MonoSingleton<GM> 
{
	#region Variables
	[SerializeField, Header("Lightning")] private GameObject lightning;
	[SerializeField] private AnimationCurve lightningAppearCurve;
	[SerializeField] private float lightningMoveSpeed = 1.5f;
	[SerializeField] private float lightningMoveDistance = 1.5f;

	[SerializeField, Space] private float speedupTime;
	[SerializeField] private float speedupValue;

	[SerializeField, Space] private Text scoreText;

	[SerializeField, Space] private TextMeshProUGUI starsText;

	private bool lightningIsVisible = false;
	private float originalTimeScale; 
	#endregion

	private void Start()
	{
		BallManager.Instance.BallsLaunched += StartCountdownTimer;
		Ball.LastBallReady += StopCountdownTimer;

		Spawner.Instance.MovedDown += OnMovedDown;

		originalTimeScale = Time.timeScale;

		starsText.text = GameData.Instance.saveData.Stars.ToString();
	}

	private void OnMovedDown()
	{
		scoreText.text = Spawner.Instance.GetScore().ToString();
	}

	private void StartCountdownTimer()
	{
		StartCoroutine("_startCountdownTimer");
	}

	private void StopCountdownTimer()
	{
		StopCoroutine("_startCountdownTimer");

		if (lightningIsVisible)
			StartCoroutine(ToggleLightning(show: false));
	}

	private IEnumerator ToggleLightning(bool show)
	{
		if (show)
			lightningIsVisible = true;
		else
			lightningIsVisible = false;

		float time = 0f;
		float curveAmount = lightningAppearCurve.Evaluate(time);
		Vector3 startPos = lightning.transform.position;

		while (time < 1f)
		{
			time += Time.deltaTime * lightningMoveSpeed;
			curveAmount = lightningAppearCurve.Evaluate(time);

			lightning.transform.position = (show ? 1 : -1) * Vector3.left * curveAmount * lightningMoveDistance + startPos;
			yield return null;
		}

		if (show)
		{
			SpeedUpTime();
		}
		else
		{
			SetNormalTime();
		}
	}

	private IEnumerator _startCountdownTimer()
	{
		yield return new WaitForSeconds(speedupTime);

		if (!lightningIsVisible)
		{
			StartCoroutine(ToggleLightning(show: true));
		}
	}

	private void SpeedUpTime()
	{
		Time.timeScale = speedupValue;
	}

	private void SetNormalTime()
	{
		Time.timeScale = originalTimeScale;
	}

	public void AddStar()
	{
		GameData.Instance.saveData.Stars++;

		starsText.text = GameData.Instance.saveData.Stars.ToString();
	}
}

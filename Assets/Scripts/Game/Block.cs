﻿using UnityEngine;
using TMPro;
using System.Collections;

[System.Serializable]
public struct BlockData
{
	public static readonly float CELL_VISUAL = 0.75f;
	public static readonly float GAP = 0.02f;
	public static readonly float startingPointY = 4.25f;

	public static readonly float startingPointX = -3.08f;
	public static readonly int BLOCKS_PER_CONTAINER = 9;

	public static float CELL_SIZE { get { return CELL_VISUAL + GAP; } }
}

[RequireComponent(typeof(SpriteRenderer), typeof(Collider2D))]
public class Block : MonoBehaviour
{
	#region Variables
	private static Transform hps;
	[SerializeField, Space] private Color[] colors;
	[SerializeField] private GameObject hpTextPrefab;

	private SpriteRenderer spriteRenderer;

	private TextMeshProUGUI hpText;
	private Animator hpTextAnimator;

	private int _hp;

	public int HP
	{
		get { return _hp; }
		set { _hp = value; }
	}
	#endregion

	private void Awake()
	{
		hps = GameObject.FindGameObjectWithTag("HPs").transform;
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void Construct(Vector2 pos, int hp)
	{
		transform.localPosition = pos;

		this.HP = hp;

		InitText();
		SetColor();

		hpTextAnimator = hpText.GetComponent<Animator>();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.CompareTag("Ball") || collision.collider.CompareTag("Green Ball"))
		{
			TakeDamage(1);
		}
	}

	public void InitText()
	{
		TextMeshProUGUI hpText = Instantiate(hpTextPrefab, hps).GetComponent<TextMeshProUGUI>();

		this.hpText = hpText;
		this.hpText.transform.position = this.transform.position;

		SetHPText(HP);
	}

	private void SetHPText(int hp)
	{
		if (hpText != null)
			hpText.text = hp.ToString();
		else
			throw new UnityException();
	}

	private void SetColor()
	{
		switch (HP / 10)
		{
			case 0:
			case 1:
			case 2: spriteRenderer.color = Color.Lerp(colors[0], colors[1], (float)HP / 30f);
				break;

			case 3:
			case 4:
			case 5: spriteRenderer.color = Color.Lerp(colors[2], colors[3], (float)HP / 30f);
				break;

			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15: spriteRenderer.color = Color.Lerp(colors[3], colors[4], (float)HP / 30f);
				break;

			default: spriteRenderer.color = colors[Random.Range(0, colors.Length / 2) * 2 + 1];
				break;
		}
	}

	private void Die()
	{
		Destroy(gameObject);
		Destroy(hpText);
	}

	public void TakeDamage(int damage)
	{
		HP -= damage;

		if (HP <= 0)
			Die();

		else
		{
			SetHPText(HP);
			SetColor();

			hpTextAnimator.SetTrigger("Hit");
		}
	}
	
	public IEnumerator MoveTextDown(float movementSpeed, Vector2 endPos)
	{
		while ((Vector2)hpText.transform.position != endPos)
		{
			hpText.transform.position = Vector2.MoveTowards(hpText.transform.position, endPos, movementSpeed * Time.deltaTime);

			yield return null;
		}
	}
}

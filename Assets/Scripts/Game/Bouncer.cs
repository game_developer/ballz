﻿using UnityEngine;

public class Bouncer : Powerup
{
	public Color newBallColor;

	protected override void UsePowerUp(GameObject ball)
	{
		ball.GetComponent<SpriteRenderer>().color = newBallColor;
		ball.GetComponent<Ball>().TouchesLeft++;
	}
}

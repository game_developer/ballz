﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Menu : MonoBehaviour
{
	[Header("Menu")]
	[SerializeField] private GameObject playButton;
	[SerializeField] private GameObject optionsButton;
	[SerializeField, Space] private TextMeshProUGUI starsText;

	[Header("Options"), Space]
	[SerializeField] private GameObject options;
	[SerializeField] private GameObject closeOptionsButton;
	[SerializeField] private GameObject soundButton;
	[SerializeField] private GameObject musicButton;
	[SerializeField] private GameObject helpButton;
	

	private void Start()
	{
		//starsText.text = GameData.Instance.saveData.Stars.ToString();
	}

	public void OnPlayClick()
	{
		SceneManager.LoadScene("Game");
	}

	public void DisplayOptions()
	{
		options.SetActive(true);
	}

	public void CloseOptions()
	{
		options.SetActive(false);
	}
}

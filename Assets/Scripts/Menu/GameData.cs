﻿using UnityEngine;

public class GameData : MonoBehaviour
{
	public static GameData Instance { get; set; }

	public SaveData saveData;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(this);
		}

		else
			Destroy(gameObject);
		
		Load();
	}

	private void Load()
	{
		if (PlayerPrefs.HasKey("Save"))
			saveData = Helper.Deserialize<SaveData>(Helper.Decrypt(PlayerPrefs.GetString("Save")));

		else
		{
			saveData = new SaveData();
			Save();
		}
			
	}

	private void Save()
	{
		PlayerPrefs.SetString("Save", Helper.Encrypt(Helper.Serialize(saveData)));
	}

	private void OnApplicationQuit()
	{
		Save();
	}
}

[System.Serializable]
public class SaveData
{
	public int Stars = 0;
	public int CheckpointLevel = 0;
}
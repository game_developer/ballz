﻿using System.Xml.Serialization;
using System.IO;
using System.Security.Cryptography;
using System;
using System.Text;
using UnityEngine;

public static class Helper
{
    private static string EncryptionKey = "!@#$%^&*()-=";

    public static string Serialize<T>(this T toSerialize)
    {
		return JsonUtility.ToJson(toSerialize);
    }

    public static T Deserialize<T>(this string toDeserialize)
    {
		return JsonUtility.FromJson<T>(toDeserialize);
    }

    /// <summary>
    /// Thanks to the man who posted it in stack overflow
    /// This is very useful functions
    /// </summary>
    /// <param name="toEncrypt"></param>
    /// <returns></returns>
    public static string Encrypt(string toEncrypt)
    {
        byte[] clearBytes = Encoding.Unicode.GetBytes(toEncrypt);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                toEncrypt = Convert.ToBase64String(ms.ToArray());
            }
        }
        return toEncrypt;
    }
    public static string Decrypt(string toDecrypt)
    {
        toDecrypt = toDecrypt.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(toDecrypt);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                toDecrypt = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return toDecrypt;
    }
}
